# ArchLinux guìa de instalaciòn con el sistema de archivos btrfs, snapper o timeshift

Bienvenidos a esta guìa escrita del proceso de instalaciòn, configuraciòn, tips y pasos posteriores de la instalaciòn de **Archlinux** para tener al final un sistema que:

1. Acceso absoluto a los snapshots, borrar, eliminar y cambiar archivos.
2. Guarde snapshots del sistema al actualizar o realizar algùn cambio.
3. Configurar la creaciòn y limpieza de los snapshots de forma automàtica, en **snapper** como en **timeshift**.
4. Como restaurar el sistema desde un punto de restauraciòn anterior, gracias a **snapper** o **timeshift**.

## Preparaciòn

### Primeros pasos

#### Descarga de archlinux:

Lo primero que necesitaremos es el iso de arch que se puede descargar desde su sitio web oficial:

- Sitio Web de descarga: [Descargar Archlinux](https://archlinux.org/download).

#### Preparar el live usb:

Tenemos muchas opciones para preparar un live usb, las màs comunes son usar utilidades con interfaz gràfica como:

- **Windows**: [rufus](https://rufus.ie/en/), [Balena Ecther](https://rufus.ie/en/).
- **MacOS**: [Balena Ecther](https://www.balena.io/etcher/).
- **Linux**: [Balena Ecther](https://www.balena.io/etcher/), [Ventoy](https://www.ventoy.net/en/download.html).

#### Entrar en el live de usb de archlinux:

Presionando la tecla **F11** en la bios o **Supr** en algunos modelos, si tienes un equipo lenovo puedes usar el **Novo button** para entrar a las opciones de configuraciòn y seleccionar el orden de arranque del computador.

## Inicio de la instalaciòn

Ahora pasaremos a la instalaciòn de **Archlinux**

#### Definir la distribuciòn de nuestro teclado:

Lo mas importante es encontrar nuestra distribuciòn de teclado en este ejemplo usare la distribuciòn española, ya que por defecto viene en ingles de Estados Unidos, haremos esto usando el siguiente comando: `loadkeys es`, **Aqui la distribuciòn ahora cambiara a español de españa, si quieres encontrar tu distribuciòn de teclado puedes consultarlo en la [Wiki](https://wiki.archlinux.org/title/Installation_guide#Set_the_keyboard_layout)**.

#### Definir el tiempo del sistema

Ahora definiremos el tiempo del sistema, se hace con el siguiente comando: `timedactl set-ntp true`.

#### Determinar los servidores de descarga màs ràpidos:

Para seleccionar los servidores de **Archlinux**, usaremos `reflector` para determinarlos para que los paquetes sean veloces en la descarga:

- Con el comando `reflector --country US --sort rate --latest 6 --save /etc/pacman.d/mirrorlist --verbose`, guardara los mirrors en el archivo mirrorlist este generara automaticamente el listado de los servidores de los cuales descargara los paquetes; Puedes cambiar el argumento `--country US` el paìs donde estès por ejemplo `--country CO` que seria en este caso Colombia.

- Con el comando `--list-countries` ves la lista de paises disponibles.

#### Particionar nuestros discos:

Usaremos el comando `lsblk` para tener siempre una descripciòn gràfica de como estàn distribuidos nuestros discos y particiones, usalo siempre al terminar estos pasos.

En la particiòn de nuestros discos podemos utilizar herramientas como `fdisk`, `gdisk`, `parted` entre otras, pero para nuestro caso usaremos `cgdisk` para crear nuestras particiones en sistemas **UEFI**.

- **Nota**: Para sistemas **MBR o Legacy Bios** se necesita un esquema de particiòn **dos** que se hace con el comando `cfdisk`.
- **Nota #2**: esta instalaciòn solo sera hecha en sistemas UEFI, si necesitas instalar tu sistema en **MBR o Legacy Bios** consulta esta guìa hecha por _[eflinux](https://www.youtube.com/watch?v=FudOL0-B9Hs)_, ya que hay algunos pasos diferentes que debes hacer para la lograr la instalaciòn, ademàs de eso puedes consultar la instalaciòn del **bootloader** descrita en su video en **ingles** por si tienes dudas.

#### Crear el formato a nuestras particiones

Para crear el formato con el comando `mkfs`:

- `mkfs.vfat /dev/sdx1`: la particiòn màs pequeña en ella irà el bootloader y el GRUB.
- `mkfs.btrfs /dev/sdx2`: la particiòn màs grande en donde estarà alojado nuestro sistema operativo.

#### Montar las particiones btrfs:

Ahora montaremos las particiones empezando siempre por la màs grande en este caso `/dev/sdx2` en `/mnt`; Como este formato es btrfs necesitamos crear los subvolumenes a esta particiòn, tambien debemos crear los directorios o puntos para el montaje de nuestros subvolumes como se aprecia en este [diagrama](https://wiki.archlinux.org/title/Snapper#Suggested_filesystem_layout).

- Usaremos el comando `btrfs subvolume create /mnt/@` para crear el root subvolume, procederemos a hacer lo mismo con los demas `@home, @var_log o @var, @snapshots`.

- **Nota**: Para timeshift no es necesario crear el `@snapshots` subvolume, solo es necesario el `@, @home, @var`.

- Desmontaremos el `/mnt` con el comando `umount /mnt` de nuestra particiòn para crear los directorios de montaje y para especificar opciones que son necesarias para el sistema de archivos **btrfs**.
- Montamos el root subvolume con las siguientes opciones: `mount -o noatime,space_cache=v2,compress=zstd,subvol=@ /dev/sdx2 /mnt`.
- Procedemos a crear los directorios: `mkdir -p /mnt/boot/efi`; Ahora los demas: `mkdir -p /mnt/{home,.snapshots}`.
  - _Tip_: La designaciòn de los nvme en linux es `nvme0n1`, para las particiones `nvme0n1p1`.
  - **Nota**: Para los ssd y nvme es recomendable poner `discard=async` en las opciones de montaje para aumentar el rendimiento en estos discos duros, ademas de dejar un poco de espacio en nuestro nvme o ssd lo beneficia en gran medida.
  - **Nota #2**: Para el `@var` o `@var_log` subvolume se debe montar en `/var` o `/var/log` en su defecto.
  - **Nota #3**: Para el `@snapshots` se monta en `/mnt/.snapshots`.
  - **Nota #4**: Puedes usar el sistema de compresion que quieras, entre los mas comunes esta `lzo, zstd`, si necesitas mas informaciòn puedes ir a [btrfs wiki](https://wiki.archlinux.org/title/Btrfs#Compression).

#### Montar la particiòn efi:

Procederemos a montar la particiòn EFI `/dev/sdx1` en `/mnt/boot/efi`.

### Instalaciòn de los paquetes principales

Teniendo nuestras particiones ahora procederemos a instalar el sistema base con el comando `pacstrap /mnt base linux linux-firmware vim git intel-ucode btrfs-progs`, si tienes un procesador **AMD** cambia el `intel-ucode` por `amd_ucode`, puedes cambiar el editor _vim_ por _nano_ si prefieres o el editor que te guste.

### Generar el archivo fstab

Se usa el comando `genfstab -U /mnt >> /mnt/etc/fstab` para generar la tabla de nuestras particiones asì como donde estàn montadas.

### Arch-chroot

Apartir de aqui es seguir la tìpica instalaciòn de Archlinux la cual puedes consultar en la [Wiki](https://wiki.archlinux.org/title/Installation_guide#Configure_the_system), pero hay apartes en los cuales se deben tener en cuanta como por ejemplo:

**Instalaciòn de snapper**: cuando instales los paquetes recuerda que snapper requiere `rsync, snap-pac y grub-btrfs`.

- `pacman -S snapper rsync snap-pac grub-btrfs `.

**mkinitcpio.conf**: En este archivo debemos agregar el modulo btrfs en la seccion de mòdulos `MODULES`, y regenerar el kernel para que tenga estos parametros con el comando `mkinitcpio -p linux`.

- `MODULES=(btrfs)`: asì quedaria nuestro mòdulo.
  - **Nota**: Si tienes una tarjeta gràfica debes agregarlo igual depende de tu marca:
  - **nvidia**: `MODULES=(btrfs nvidia)`
  - **AMD**: `MODULES=(btrfs amdgpu)`
  - **Intel**: `MODULES=(btrfs i915)`

### Conectarse a la red

La red es automaticamente detectada si tienes LAN, pero si tienes wifi debes usar `nmtui` selecciona la red y poner tu contraseña.

## Configuracion de snapper con snapper-gui

### Crear el archivo de configuraciòn para snapper - _recuerda usar sudo_

#### Crear archivo de configuraciòn:

Empezaremos con crear el archivo de configuraciòn para nuestro snapper:

- Ya que tenemos el directorio `/.snapshots` debemos seguir el siguiente procedimiento:

  > Desmontar y eliminar el directorio /.snapshots, generar el archivo de configuraciòn con el comando que provee la [Snapper Wiki](https://wiki.archlinux.org/title/Snapper#Creating_a_new_configuration), crear el directorio una vez màs y posteriormente volverlo a montar, ademas de eso eliminar el @snapshots subvolume ya que el comando anterior nos crea un `@snapshots` subvolume por defecto, lo veremos en detalle a continuaciòn:

  - **Desmontar el directorio /.snapshots**: `umount /.snapshots/`
  - **Eliminar el directorio**: `rm -r /.snapshots`
  - **Crear el archivo de configuraciòn**: `snapper -c root create-config /`
  - **Eliminar el subvolume creado por nosotros**: `btrfs subvolume delete /.snapshots`
  - **Crear el nuevo directorio /.snapshots**: `mkdir /.snapshots`
  - **Montar el nuevo directorio /.snapshots en @snapshots subvolume**: `mount -a`

Debemos cambiar los permisos de nuestra carpeta creada con la configuracion de snapper para esto usamos el comando `chmod 750 /.snapshots`

- **Nota** : Solo el usuario root tendra los permisos de acceso a la carpeta /.snapshots.

#### Permitir el acceso de nuestro usuario a la configuraciòn de snapper:

Para cambiar el acceso para nuestro usuario debemos modificar el archivo de configuraciòn que creamos con snapper el cual se encuentra en: `/etc/snapper/configs/`.

- Agregamos el nuestro en nuestro snapper config en la linea 21: `ALLOW_USERS=username`.
  - **Nota**: debes cambiar `username` por el nombre de usuario creado en arch-chroot por ejemplo: `ALLOW_USERS=david`.

#### Modificando el limite de los snapshots:

Definir la cantidad de snapshots en el sistema es importante, en el archivo de configuraciòn podemos modificar este parametro desde la linea 49 en nuestro archivo de configuraciòn de snapper, si quieres un ejemplo de esta configuraciòn puedes ir a la [Snapper Wiki](https://wiki.archlinux.org/title/Snapper#Set_snapshot_limits), pero puedes modificarla segùn tus necesidades.

#### Activando los servicios de snapper:

Debemos activar los servicios de snapper, `timeline` y el `cleanup` con los siguientes comandos, esto asegura el correcto funcionamiento limpieza de nuestros snapshots:

- `systemctl enable --now snapper-timeline.timer`
- `systemctl enable --now snapper-cleanup.timer`
- `systemctl enable --now grub-btrfs.path` => Para darle seguimiento a los snapshots y mostrarlos en el grub

#### Instalaciòn de snapper-gui:

Ahora instalaremos, snapper-gui puedes hacerlo con yay, paru o pikaur o con el aur helper que mas te guste.

#### Crear snapshots de nuestras particiones que no son btrfs:

Crear el backup para nuestra particiòn efi es importante, para ello debemos crear un **hook**, lo haremos de la siguiente manera:

- **Creamos el directorio donde estara el archivo hook**: `mkdir /etc/pacman.d/hooks`
- **Creamos el archivo 50-bootbackup.hook en el directorio**: `vim 50-bootbackup.hook`
- **Escribimos la configuraciòn adentro del archivo**: [Snapper wiki](https://wiki.archlinux.org/title/Snapper#Backup_non-Btrfs_boot_partition_on_pacman_transactions)
  - **Nota**: debes cambiar el `Target` por la **ubicaciòn de los archivos UEFI**, ejemplo => `Target = boot/*`

#### Modificar los permisos de la carpeta /.snapshots

Por ultimo debemos cambiar los permisos a nuestra carpeta /.snapshots para nuestro usuario, ademàs de los permisos de nuestra carpeta, lo hacemos con los siguientes comando:

- `chmod a+rx /.snapshots` => _Cambiar los permisos de la carpeta_
- `chown :username /.snapshots` => _Cambiar los permisos de usuario_.

### Restaurar los snapshots con snapper

#### Propiedades de los snapshots:

Otra parte muy importante es como saber como poder restaurar nuestro sistema, aquì mostraremos los pasos para poderlo realizar de forma segura y probar de manera efectiva los snapshots que tenemos creado en nuestro sistema.

### Snapper CLI

Veremos primero como se hace desde la terminal, estos son los comandos bàsicos, pero si quieres ver las opciones disponibles que tiene snapper puedes consultarlo en la [Snapper Wiki](https://wiki.archlinux.org/title/Snapper#Managing_snapshots).

- **Listar nuestros snapshots**: `snapper -c root list`
- **Crear a snapshot manualmente**: `snapper -c root create -c timeline --description NAMEOFSNAPSHOTS` => Define el nombre que mas te guste.

  - **Nota**: Los snapshots creados por snapper y snapper_gui son solo de lectura

Debemos cambiarle las propiedades a nuestros snapshots ya que solo son de lectura para volverlos _lectura y escritura_ lo heremos con el siguiente comando:

- **Verificar el snapshot**: `btrfs property list -ts /.snapshots/SNP#00/snapshot` => Verificamos si el status

- **Cambiar de ro a rw**: `btrfs property set -ts /.snapshots/SNP#00/snapshot ro false` => _SNP#00_: numero del snapshot a cambiar
  - Puedes consultar tambien como hacerlo en la [Snapper Wiki](https://wiki.archlinux.org/title/Snapper#Restoring_/_to_its_previous_snapshot) y en [Stack Exchange](https://unix.stackexchange.com/questions/149932/how-to-make-a-btrfs-snapshot-writable/149933#149933).

#### Restaurar un snapshot desde el live iso de ArchLinux

Este es un procedimiento que podemos hacer para la restauraciòn de nuestro sistema lo haremos siguiendo estos pasos:

- **Entrar en el live iso**: Entra al live iso normalmente como si fueras a instalar ArchLinux.
- **Montamos la particiòn donde tenemos el sistema de archivos btrfs**: `mount /dev/sdx2 /mnt`.
- **Buscar que snapshot necesitamos**: `nano /mnt/@snapshots/*/info.xml` => para navegar entre los archivos se hace con: _alt + >_, para salir: _ctrl + x_
- **Borramos el @subvolume**: `rm -rf /mnt/@` => desde el kernel 3.6, puedes eliminar subvolume como si fuera un archivo
- **Montar el snapshot desdeado**: `btrfs subvolume snapshot /mnt/SNP#00/snapshot /mnt/@`
- **Reiniciamos el sistema**

## Timeshift, Timeshift autosnap

En esta parte veremos la implementaciòn de timeshift y timeshift-autosnap ambos disponibles en AUR, para administrar nuestros snapshots de una manera mas facìl, snapper toma tiempo para configurarlo pero con esta forma es màs sencilla.

- **Nota**: Timeshift solo permite crear snapshots del `@` subvolume y `@home` subvolume por lo que otros esquemas no estan permitidos para mas informaciòn puede ver la documentaciòn de [Timeshift](https://github.com/teejee2008/timeshift#readme).

### Instalaciòn de timeshift y timeshift autosnap

Puedes usar `yay, paru o pikaur`, para instalar timeshift y timeshift autosnap desde el repositorio de AUR.

### Bootear desde un snapshot

Gracias a grub-btrfs ellos incluyen una forma de generar estos snapshots automaticamente, puedes verlo en la documentaciòn del repositorio de [grub-btrfs](https://github.com/Antynea/grub-btrfs/blob/master/initramfs/readme.md).

- Debes poner en el hooks del mkinitcpio.conf el `grub-btrfs-overlayfs` y recrear la imagen con `mkinitcpio -P`:
  - Quedara asi `HOOKS=(base udev autodetect modconf block filesystems keyboard fsck grub-btrfs-overlayfs)`.

### Restaurar snapshots usando timeshift

En este caso usaremos timeshift para restaurar nuestro sistema:

#### Restaurar con Timeshift GUI:

Puedes crear un live de cualquier distribuciòn de linux, instalar timeshift en el live usb y automaticamente timeshift detectara nuestros snapshots creados con la aplicaciòn, con lo cual solo seria seleccionar el snapshot que queremos restaurar y darle al botòn de restaurar.

- La distribuciòn mas recomendable es [Linux Lite](https://www.linuxliteos.com/download.php), ya que esta contiene timeshift instalado en su live iso.

## Script para configurar los snapshots de snapper o timeshift

Hice un pequeño script que configura snapper y timeshift para ahorrar tiempo, lo puedes encontrar en este repositorio con el nombre de `snapshots_setup.sh` ya contiene los permisos de ejecuciòn, ademàs puedes modificarlo a tu gusto, _viva el opensource_.

## Referencias

Puedes encontrar toda la informaciòn en las siguientes referencias, agradecimientos a estas personas que ponen su conocimiento al alcance de nosotros:

**EFlinux**:

- [eflinux channel](https://www.youtube.com/channel/UCX_WM2O-X96URC5n66G-hvw)
- [Arch Linux: Monthly Install - 05.2021](https://www.youtube.com/watch?v=dohxv1ULcL4) _by eflinux_
- [Arch Linux Install: January 2021 ISO With BTRFS & Snapshots](https://www.youtube.com/watch?v=Xynotc9BKe8) _by eflinux_
- [How to install Arch Linux with BTRFS & Snapper](https://www.youtube.com/watch?v=sm_fuBeaOqE) _by eflinux_

**ArchLinux**:

- [Archlinux](https://archlinux.org)
- [ArchWiki](https://wiki.archlinux.org/)
- [Snapper Wiki](https://wiki.archlinux.org/title/Snapper)

**Stack Exchange**

- [How to make a btrfs snapshot writable](https://unix.stackexchange.com/questions/149932/how-to-make-a-btrfs-snapshot-writable/)

**Timeshift by teejee2008**

- [Timeshift](https://github.com/teejee2008/timeshift#readme)

**grub-btrfs by Antynea**

- [grub-btrfs](https://github.com/Antynea/grub-btrfs#readme)

**Nota**: _Todos los derechos pertenecen a sus respectivos autores, por lo tanto esta guìa no trata de infringir los derechos de autor, este documento esta proyectado para uso educativo, en las referencias tienen la informaciòn de todos las fuentes que participaron en la construcciòn de este documento, asì mismo con sus respectivas licencias._

_Oku &copy; 2021_.
