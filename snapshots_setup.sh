#!/bin/bash

exit=0

snapper_setup () {
    echo "Setting up snapper"
    sudo umount /.snapshots
    sudo rm -r /.snapshots
    sudo snapper -c root create-config /
    sudo btrfs subvolume delete /.snapshots
    sudo mkdir /.snapshots
    sudo mount -a
    sudo chmod 750 /.snapshots
    echo "Put your user in to the config file (ALLOW_USERS="yourusername") and config your cleanup schedule"
    sleep 5
    sudo vim /etc/snapper/configs/root
    echo "Enabling services"
    sleep 2
    sudo systemctl enable --now snapper-timeline.timer
    sudo systemctl enable --now snapper-cleanup.timer
    sudo systemctl enable --now grub-btrfs.path
    echo "Creating the hook for the boot partition"
    sleep 3
    sudo mkdir /etc/pacman.d/hooks
    sudo vim /etc/pacman.d/hooks/50-bootbackup.hook
    echo "Setting up permisions"
    sleep 3
    sudo chmod a+rx /.snapshots
    sudo chown :oku /.snapshots
    echo "Installing snapper gui"
    sleep 3
    git clone https://aur.archlinux.org/yay
    cd yay
    makepkg -si
    yay -S snapper-gui
    echo "Set up complete ... reboot the system"
}

timeshift_setup (){
    echo "Setting up timeshift"
    git clone https://aur.archlinux.org/yay
    cd yay
    makepkg -si
    yay -S timeshift timeshift-autosnap
    echo "Put the hook grub-btrfs-overlayfs into mkinitcpio.conf"
    sleep 3
    sudo vim /etc/mkinitcpio.conf
    sudo mkinitcpio -P
    echo "Set up complete ... reboot the system"
}

while [ $exit -eq 0 ]
do
    echo "Welcome to setup snapshots for snapper and timeshift"
    echo "by Oku"
    echo "1) Snapper Instalation"
    echo "2) Timeshift Instalation"
    echo "3) Exit"

echo "Select a option"
read option

case $option in 
    1) snapper_setup;;
    2) timeshift_setup;;
    3) exit=1;
esac
done
