# Archlinux guìa de instalaciòn para sistemas btrfs

Hola y bienvenido a este repositorio, la decisiòn de crear esta guìa es de tener toda la informaciòn disponible de grandes personas en un solo lugar, ademàs de contar con pasos para la instalaciòn de archlinux siguiendo este formato en el cual contiene lo siguiente:

- Guìa escrita del proceso de configuraciòn, instalaciòn y tips para el correcto funcionamiento del sistema de archivos btrfs en Archlinux.
- Un script que permite configurar snapper o timeshift segùn tus preferencias.

> Si tienes dudas, comentarios o apuntes puedes contactarme en **OkuNull@protonmail.com**

Oku &copy; 2021.
